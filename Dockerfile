FROM ruby:3.3.0-alpine as production

WORKDIR /app

RUN apk add build-base git less vim

# Install gems
COPY Gemfile* /app/
RUN gem update bundler && bundle install --jobs "$(nproc)" --retry 2

COPY . /app

CMD /bin/sh

