#!/usr/bin/env ruby

require "csv"
require "debug"
require "json"
require "net/http"
require "time"
require "uri"

class Base
  attr_reader :gitlab_token, :report_name
  GITLAB_GRAPHQL_API = "https://gitlab.com/api/graphql".freeze
  GITLAB_PROJECTS = %w[ucsdlibrary ucsdlibrary/development ucsdlibrary/devops surfliner chronopolis].freeze

  def initialize
    @gitlab_token = ENV.fetch("GRAPHQL_TOKEN") # intentionlly throw KeyError if not present
    @report_name = "repo_summary_#{Time.now.strftime("%Y-%m-%d")}.csv"
  end
end

class GitlabReport < Base
  def project_query(project_path)
    uri = URI.parse(GITLAB_GRAPHQL_API)
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/json"
    request["Authorization"] = "Bearer #{gitlab_token}"
    request.body = JSON.dump({
      "query" => "query {group(fullPath: \"#{project_path}\") {projects {nodes {name description webUrl createdAt lastActivityAt}}}}"
    })

    req_options = {
      use_ssl: uri.scheme == "https"
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end
    JSON.parse(response.body)["data"]["group"]["projects"]["nodes"]
  end

  def run
    CSV.open(report_name, "wb") do |csv|
      csv << ["Name", "Description", "URL", "Created At", "Last Activity At"]
      GITLAB_PROJECTS.each do |project_path|
        project_query(project_path).each do |project|
          csv << [project["name"],
            project["description"],
            project["webUrl"],
            project["createdAt"],
            project["lastActivityAt"]]
        end
      end
    end
  end
end

GitlabReport.new.run
