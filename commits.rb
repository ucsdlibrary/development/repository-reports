#!/usr/bin/env ruby

require "English"
require "debug"
require "fileutils"
require "gitlab"
require "octokit"
require "time"
require "yaml"

# set longer read timeout, Gitlab sometimes stalls
# see: https://github.com/NARKOZ/gitlab/issues/357
ENV["GITLAB_API_HTTPARTY_OPTIONS"] = "{read_timeout: 60}"

# expect start and end date parameters
start_date = ARGV[0]
end_date = ARGV[1]

unless start_date && end_date
  raise "Please supply both a start and end date for the report.."
end
#
# simple date validation

[start_date, end_date].each do |d|
  Date.parse(d)
end

class Base
  attr_reader :start_date, :end_date, :config, :client, :report

  def initialize(start_date, end_date)
    @start_date = start_date
    @end_date = end_date
    @config = YAML.safe_load_file("config.yml")
    @report = File.open("commits_#{start_date}-#{end_date}.report", "w")
  end
end

class GitlabReport < Base
  def configure
    Gitlab.configure do |c|
      c.endpoint = config["gitlab"]["endpoint"]
      c.private_token = config["gitlab"]["personal_access_token"]
    end
  end

  def run
    configure
    group_ids.each do |id|
      group_projects(id).auto_paginate do |project|
        commits(project)
      end
    end
  ensure
    report.close
  end

  def group_ids
    config["gitlab"]["group_ids"]
  end

  def group_projects(group_id)
    Gitlab.group_projects(group_id)
  end

  # see https://www.rubydoc.info/gems/gitlab/Gitlab/Client/Commits#commits-instance_method
  def commits(project)
    total = 0
    Gitlab.commits(project.id,
      {since: start_date,
       until: end_date,
       per_page: 100}).auto_paginate do |c|
      total += filtered_commit_value(c)
    end
    report.puts "#{project.name} #{total}" unless total == 0
  end
end

# We only want commits from UCSD staff
# This is unfortunatly the most accurate way of checking for this as the Gitlab API does not provide User identifiers in
# the results of each `Gitlab.commit` response. So there is nothing to validate against other than the email suffix
def filtered_commit_value(commit)
  if commit.author_email.end_with? "ucsd.edu"
    1
  else
    0
  end
end

class GitHub < Base
  def client
    @client = Octokit::Client.new(login: config["account"]["user"],
      password: config["account"]["password"])
    @client.login
    @client.auto_paginate = true
  end

  def commits
    # TBD
  end
end

GitlabReport.new(start_date, end_date).run
