# UC San Diego Library GitHub Reports #

The reports contained in this repository are used for keeping track of the
contributions made by the UC San Diego Library development team in both local
and open source/community projects on GitHub.

## Setup ##
1. You will need a current version of `docker` or `podman`
1. Clone repository `git clone https://gitlab.com/ucsdlibrary/development/repository-reports.git`
1. `cd repository-reports`
1. Setup `config.yml` as follows:
    1. `cp config.yml.sample config.yml`
    1. Add your GitHub username and password to the `user` and `password`
       fields, replacing the existing boilerplate defaults
    1. Create a [personal access token for Gitlab][pac-gitlab], check the `api`
       Scope checkbox when creating it. Add this token to LastPass, or whatever
       fits your workflow
    1. Add your Gitlab personal access token to the `personal_access_token`
       field, replacing the existing boilerplate defaults
1. `docker build -t git-reports .`
1. Run report(s) below as needed

You can also open a terminal in the container directly if you prefer:

```sh
docker run -it --rm --volume "`pwd`:/app" git-reports /bin/sh
```

## Reports ##

### Repository Summary
Where: `repo_summary.rb`
What: A rollup summary of all git repositories. Used by management as an inventory references.
Needed: A `GRAPHQL_TOKEN` environment variable with your [personal access token][pac-gitlab] for Gitlab

```
docker run -it --env GRAPHQL_TOKEN=your-gitlab-token --rm --volume "`pwd`:/app" git-reports ruby repo_summary.rb
```

A csv will be created using the current date in `yyyy-mm-dd` format. Example: `repo_summary_2024-04-08.csv`

### Open Issues
Where: `open_issues.rb`
What: A list of all tickets assigned to members of the development team that are
currently `open`

To Use:
1. `docker run -it --rm --volume "`pwd`:/app" git-reports ruby open_issues.rb`
2. Open up the generated `open_issues.report` page and do a select all +
   copy
3. Follow instructions in [Embedding Report Data in
   Confluence](#embedding-report-data-in-confluence) section

### Closed Issues
> This is no longer in use but kept for reference
> Issues are reassigned for QA from the development team, so we can't query for this anymore

Where: `closed_issues.rb`
What: A list of closed issues between a specified time frame. Usually a 2 week
Sprint, but could be for a month, year, etc.

To Use:
1. This script expects two arguments. A `start_date` and an `end_date`. These
   should be formatted as follows `YYYY-MM-DD`. Example: `2017-12-01`
1. `docker run -it --rm --volume "`pwd`:/app" git-reports ruby closed_issues.rb 2017-12-01 2017-12-31`
More examples:
    1. 2 week period: `2017-12-01 2017-12-15`
    1. 4 week period: `2017-12-01 2017-12-31`
    1. Year (this will be SLOW): `2017-12-01 2018-12-01`
1. Open up the generated `closed_issues<date-range>.report` page and do a select all +
   copy. An example filename: `closed_issues_2017-12-01-2017-12-31.report`
1. Follow instructions in [Embedding Report Data in
   Confluence](#embedding-report-data-in-confluence) section

### Commits
Where: `commits.rb`
What: A list of commits between a specified time frame. Tends to be yearly
report for managers.
1. This script expects two arguments. A `start_date` and an `end_date`. These
   should be formatted as follows `YYYY-MM-DD`. Example: `2017-12-01`
1. `docker run -it --rm --volume "`pwd`:/app" git-reports ruby commits.rb 2017-12-01 2017-12-31`


## Embedding Report Data in Confluence ##
Confluence supports embedding wiki markup, which is how we get our generated
report data into a page. [This is documented by Atlassian][markup] if you would like
further information.

1. Open Confluence/LiSN and open (or create) the page you wish to enter the
   report data
2. Click the (+) icon and choose `Insert Markup`
3. Ensure the `Confluence Wiki` option is selected
4. Paste in the report data and ensure it looks correct in the Preview pane
5. Click `Insert` to put embed the report data into the page
6. Edit the page as needed and save

[markup]:https://confluence.atlassian.com/doc/confluence-wiki-markup-251003035.html#ConfluenceWikiMarkup-Tables
[pac-gitlab]:https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token
